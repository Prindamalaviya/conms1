import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import authors from './components/authors.vue'
import books from './components/books.vue'
import insert from './components/insert.vue'
import display from './components/authors.vue'
// import login from './components/login.vue'
import update from './components/update.vue'

Vue.use(VueResource)
Vue.use(VueRouter)

// export so we can use in components
export var router = new VueRouter()

// define routes
router.map({
	'/': {
		component: login
	},
	'/authors': {
		component: insert
	},
	'/authors_dis': {
		component: authors
	},
	'/books_dis': {
		component: books
	},
	'/book/:id': {
		component: update
	}
})

// fallback route
router.redirect({
	'*': '/'
})

// expose the whole thing on element with 'app' as an id
router.start(MainTemplate, '#app')
