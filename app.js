const cookieparser = require('cookie-parser');
// const multer = require('multer');
const express = require('express');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');
const formidable = require('formidable');
const path = require('path');
const http = require('http');
const cors = require('cors');
const methodOverride = require('method-override');
const router = express.Router();
const authors = require('./server/controller/authors');
const books = require('./server/controller/books');

app.use(cors());

//create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieparser());
app.set('port', process.env.PORT || 8000);

app.use(express.static(__dirname + '/public'));

//define authors routes
router.get('/authors_dis', authors.index);

// define books routes
router.get('/books_dis', books.index);

// register api routes
 app.use('http://localhost:8000', router);

// seed the database
require('./server/seeders');






//PORT LISTENING CODE
app.listen(app.get('port'), function() {
    console.log('server started on port', app.get('port'));
});
