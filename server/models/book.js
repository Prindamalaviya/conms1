'use strict';
module.exports = function(sequelize, DataTypes) {
    var Book = sequelize.define('Book', {
        title: DataTypes.STRING,
        publication_date: DataTypes.DATE,
        discription: DataTypes.TEXT,
        author_id: DataTypes.INTEGER
    }, {
        underscored: true,
        classMethods: {
            associate: function(models) {
                Book.belongsTo(models.Author, {});
            }
        }
    });
    return Book;
}