module.exports = [
    {
        "model": "Book",
        "data": {
            "title": "xyz",
            "publication_date": new Date(1993, 9, 1),
            "discription": "hardik@123",
            "author_id": "2"
        }
    },
    {
        "model": "Book",
        "data": {
            "title": "abc",
            "publication_date": new Date(1994, 4, 23),
            "discription": "prinda@123",
            "author_id": "1"
        }
    }];