Book = require('../models').Book;

module.exports = {
    index(req, res) {
        Book.findAll()
        .then(function (books) {
            res.status(200).json(books);
        })
        .catch(function (error) {
            res.status(500).json(error);
        });
    }
}