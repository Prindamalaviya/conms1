Author = require('../models').Author;
Book = require('../models').Book;

module.exports = {
    index(req, res){
        Author.findAll({
            //return the books associated with each author
            include: Book
        }).then(function(authors){
            sendResult(res, authors);
        }).catch(function(error){
            sendError(res, error);
        })
    }
}