module.exports = {
    development: {
        url: 'vue+node',
        database: 'vue+node',
        dialect: 'mysql',
        username: 'root',
        password: 'prinda',
        port: '3306'
    },
    production: {
        url: process.env.DATABASE_URL,
        dialect: 'mysql'
    },
    staging: {
        url: process.env.DATABASE_URL,
        dialect: 'mysql'
    },
    test: {
        url: process.env.DATABASE_URL || '',
        dialect: 'mysql'
    }
};